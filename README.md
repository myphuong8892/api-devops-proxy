# APP API Proxy

NGNIX proxy app for django API

## Usage

### Environment Variables

- `LISTEN_PORT`- Port to listen on (default: `8000`)
- `APP_HOST`- Host name of the app to forwards requests to (default: `app`)
- `APP_PORT`- Port of the app to forwards requests to (default: `9000`)
